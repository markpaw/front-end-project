import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodolistComponent } from './todolist/todolist.component';
import { TodoitemComponent } from './todoitem/todoitem.component';
import { SplashComponent } from './splash/splash.component';
import { LoginComponent } from './login/login.component';
import { TodoAddComponent } from './todoadd/todoadd.component';
import { AuthService} from './auth.service';

const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { path: 'splash', component: SplashComponent },
  { path: 'login', component: LoginComponent },
  { path: 'list', component: TodolistComponent },
  { path: 'add', component: TodoAddComponent, canActivate: [AuthService] },
  { path: 'edit/:index', component: TodoitemComponent, canActivate: [AuthService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
