import { Component, HostBinding } from '@angular/core';
import { ToastService } from './toast.service';
import { Animations } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ Animations.page ]
})
export class AppComponent {

 @HostBinding('@routeAnimation') 
 anyProperty = 'anything';

   constructor(private toaster:ToastService) { 
    this.toaster.getToastListener().subscribe(
      (message:string)=>{
        this.displayToast(message,2300);
      }
    )
   }

   showToast: boolean = false;
   toastMessage: string = 'default toast message';
   title = 'app';

  displayToast(message: string, duration: number){
    this.toastMessage = message;
    this.showToast = true;
    let token = setInterval(
      ()=>{
        this.toastMessage = 'default message';
        this.showToast = false;
        clearInterval(token);
      },
      duration
    )
  }

}
