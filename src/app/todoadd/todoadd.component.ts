import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { TodoService } from '../todo.service';
import { Todo , getNewTodo } from '../Todo';

@Component({
  selector: 'app-todoadd',
  templateUrl: `./todoadd.component.html`,
  styleUrls: ['./todoadd.component.css']
})
export class TodoAddComponent implements OnInit {
  @Output() newtodo = new EventEmitter();
  todotext: string = '';
  item:Todo = getNewTodo();

  constructor(private todoSrv: TodoService, private router: Router) { }

  ngOnInit() {
  }

  onAdd() {
    this.todoSrv.addTodo(this.item).subscribe(
      (response) => {
        const result = response;
        this.router.navigate(['\list']);
      }
      );
  }

  onCancel() {
    this.router.navigate(['\list']);
  }

}
