import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-login',
  template: `
  <div class="login app-height">
    <h1>Login</h1>
    <img class=image src='/assets/images/splash.png' />
    <div *ngIf="action==='login'" class=button>
      <button (click)='onLoginGuest()' >LoginGuest</button>
      <br/>
      <button (click)='onLoginAdmin()' >LoginAdmin</button>
    </div>
    <div *ngIf="action==='logout'"  class=button>
      <button (click)='onLogout()' >Logout</button>
    </div>    
  </div>
    `,
  styles: [`
  .login{background-color:cadetblue;padding-top:25px;padding-left:30px;}
  .image{display:block; height:200px;}
  .button{margin-top:5px;}
  `
  ]
})
export class LoginComponent implements OnInit {

  action: string = 'login';

  constructor(private auth: AuthService,
    private router: Router,
    private toaster: ToastService) { }

  ngOnInit(): void {
    this.auth.getUserAsync().subscribe(
      (user)=>{ 
        if (user === 'none') {
          this.action = 'login';
        } else {
          this.action = 'logout';
        }
      }
    )

    // if (this.auth.getUser() === 'none') {
    //   this.action = 'login';
    // } else {
    //   this.action = 'logout';
    // }
  }

  onLogout() {
    let message = this.auth.logout();
    this.toaster.sendToast(message);
    this.router.navigate(['/login'])
  }

  onLoginGuest() {
    let message = this.auth.login('guest', 'guest');
    this.toaster.sendToast(message);
    this.router.navigate(['/list'])
  }
  
  onLoginAdmin() {
    let message = this.auth.login('admin', 'admin');
    this.toaster.sendToast(message);
    this.router.navigate(['/list'])
  }

}
