import { ThisReceiver } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {
  user: string = 'none';
  userAsync!: Subscriber<string>;

  constructor(private toaster:ToastService) { 
  }

  getUser() {
    return this.user;
  }

  private setUser(user:string){
    this.user = user;
    this.userAsync.next(this.user);
  }

  getUserAsync(){
    return new Observable(
      (subscriber)=>{
        this.userAsync = subscriber;
        this.userAsync.next(this.user);
      }
    )
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.user === 'admin') {
      return true;
    } else {
      this.toaster.sendToast('This feature is only available when logged-in as Admin');
      return false;
    }
  }

  logout(){
    // this.user = 'none';
    this.setUser('none');
    return `User has been logged out.`;    
  }

  login(user: string, pass: string) {
    if (user === 'admin' && pass === 'admin') {
      // this.user = user;
      this.setUser(user);
      return `user ${user} logged in.`;
    }
    if (user === 'guest' && pass === 'guest') {
      // this.user = user;
      this.setUser(user);
      return `user ${user} logged in.`;
    }

    return `invalid user ${user}!`;
  }


}
