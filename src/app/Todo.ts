export class Todo {
    'id': number;
    'title': string;
    'description': string;
    'due': string;
    'done': boolean;
}

export function getNewTodo(): Todo{
    return {...newTodo};
}

let newTodo:Todo = {
    id: -1,
    title: "",
    description: "",
    due: "",
    done: false
};