import { Component, OnInit } from '@angular/core';
import { RouterLink }  from '@angular/router';

@Component({
  selector: 'app-splash',
  template: `
  <div class="splash app-height" >
    <H1>Splash</H1>
    <img class=image src='/assets/images/splash.png' />
    <button [routerLink]="['/login']" >Next</button>
  </div>
    `,
  styles: [`
  .splash{background-color:cadetblue;padding-top:25px;padding-left:30px;}
  .image{display:block; height:200px;}
  button{margin-top:5px;}
  `
  ]
})
export class SplashComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
