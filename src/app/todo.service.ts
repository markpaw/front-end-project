import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Todo } from './Todo';


@Injectable()
export class TodoService {

  constructor(private http: HttpClient ) {}
  // baseUrl: string = "http://localhost:3000/todos";
  baseUrl: string = "http://localhost:8080/tasks";

  getTodoItems() {
    return this.http.get(`${this.baseUrl}`);
  }

  getTodoItem(id:any) {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  addTodo(todo: Todo) {
    let body = new Todo();
    body = {...todo};
    const headers = new HttpHeaders( {'Content-Type' : 'application/json' } );
    const opts:any = {};
    opts.headers = headers;
    return this.http.post(`${this.baseUrl}`, body, opts);
  }

  updateTodo(todoItem: any ) {
    const body = JSON.stringify(todoItem);
    const headers = new HttpHeaders( {'Content-Type' : 'application/json' } );
    const opts:any = {};
    opts.headers = headers;
    const url = `${this.baseUrl}/${todoItem.id}`;
    return this.http.put(url, body, opts);
  }

  deleteTodo(todoItem: Todo ) { 
    const url = `${this.baseUrl}/${todoItem.id}`;
    return this.http.delete(url);
  }

}
