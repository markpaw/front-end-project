import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  messenger!: Subscriber<string>;
  message_observable!: Observable<string>

  constructor() { 
    this.message_observable = new Observable((subscriber)=>{
      this.messenger = subscriber;
    });
  }

  getToastListener(){
    return this.message_observable;
  }

  sendToast(message: string){
    if(message && message.length > 0){
      this.messenger.next(message);
    }
  }
}
