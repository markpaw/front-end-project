import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { TodoService } from '../todo.service';
import { Todo } from '../Todo';
import { AuthService } from '../auth.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css'],
  providers: [TodoService]
})
export class TodolistComponent implements OnInit {
  @Input() title: string = 'Task List';
  searchTerm: string = '';
  applySearch: boolean = true;
  showCompleted: boolean = false;
 
  todolist:Todo[] = [];

  constructor(private todoSvc: TodoService, 
    public auth:AuthService, 
    private aroute:ActivatedRoute,
    private toaster:ToastService) {}

  ngOnInit() {
    this.showCompleted = this.toBool(window.localStorage.getItem('showcomp'));
    this.applySearch = this.toBool(window.localStorage.getItem('apply'));
    this.updateList();
  }

  toBool(value:string|null):boolean{
    value = (value === null )?'false':value;
    return (value === 'true')?true:false;
  }

  onChange(event:Event){
    let target:any = event.currentTarget;
    let key = target['id'];
    let value = target['checked'];
    window.localStorage.setItem(key,value);
  }

  updateList() {
    this.todoSvc.getTodoItems().subscribe(
      {
        "next": (response: any) => {
          this.todolist = response.sort(
            (a: any, b: any) => (a.id - b.id)
          );
        },
        "error":
          error => {
            let message = error + ' There was a problem getting data.';
            this.toaster.sendToast(message)
          }
      }
    )
  }

  onNewTodo(event:any) {
    this.updateList();
  }
}
